<h1 style="text-align:center">
Multitask Metamodel for Keypoint Visibility Prediction in Human Pose Estimation
</h1>
<div style="text-align:center">
<h3>
<a href="https://liris.cnrs.fr/page-membre/romain-guesdon">Romain Guesdon</a>,
<a href="https://liris.cnrs.fr/page-membre/carlos-crispim-junior">Carlos Crispim-Junior</a>,
<a href="https://liris.cnrs.fr/page-membre/laure-tougne">Laure Tougne</a>
<br>
<br>
International Joint Conference on Computer Vision, Imaging and Computer Graphics Theory and Applications (VISAPP) 
</h3>
</div>

# Table of content
- [Overview](#overview)
- [Installation](#installation)
- [Testing](#valid-visibility-module)
- [Training](#training-visibility-module)
- [Citation](#citation)
- [Acknowledgements](#acknowledgements)

# Overview
This repository contains the materials presented in the paper
[Multitask Metamodel for Keypoint Visibility Prediction in Human Pose Estimation]().

![Metamodel](assets/metamodel.png)

Our code is based on the [Simple Baseline](https://github.com/microsoft/human-pose-estimation.pytorch) pytorch implementation.
However, several modifications have been made from the original code to allow the implementation of our metamodel.

# Installation
- Clone this repository
- Follow the instructions on the [Simple Baseline repository](https://github.com/microsoft/human-pose-estimation.pytorch).
- Download the DriPE dataset [here](https://gitlab.liris.cnrs.fr/aura_autobehave/dripe) and place it in the `data/` directory.


# Valid visibility module
You can download pretrained weights [here](http://dionysos.univ-lyon2.fr/~ccrispim/VisPred/models).
For example, run:
```
python pose_estimation/valid.py \
    --cfg experiments/coco/resnet50/256x192_vis_freeze.yaml \
    --flip-test \
    --model-file models/pytorch/pose_coco/coco_vis2_raise_soft.pth.tar
```

# Training visibility module
You can download pretrained weight on the base model [here](http://dionysos.univ-lyon2.fr/~ccrispim/VisPred/models/coco_vis2_0_no_linear.pth.tar).
Place this file in `models/pytorch/resnet50_vis`.
Then, run:
```
python pose_estimation/train.py \
    --cfg experiments/coco/resnet50/256x192_vis_freeze.yaml
```


##### **HPE on the COCO 2017 validation set.**
AP OKS (\%) | AP | AP<sup>50</sup> | AP<sup>75</sup> | AP<sup>M</sup> | AP<sup>L</sup> | AR | AR<sup>50</sup> | AR<sup>75</sup> | AR<sup>M</sup> | AR<sup>L</sup>
:--- | :---: | :---: | :---: | :---: | :---: | :---: |:---: | :---: | :---: | :---: | 
SBl | 72 | 92 | 79 | 69 | 76 | 75 | 93 | 82 | 72 | 80

##### **Visibility prediction on the COCO 2017 validation set.**
F1 score | non-labeled | non-visible | visible | total
:--- | :---: | :---: | :---: | :---: |
SBl | 0.77 | 0.37 | 0.80 | 0.76


# Citation
If you use our network or our code, please cite:
```
@InProceedings{Guesdon_2022_Visapp,
    author    = {Guesdon, Romain and Crispim-Junior, Carlos and Tougne, Laure},
    title     = {Multitask Metamodel for Keypoint Visibility Prediction in Human Pose Estimation},
    booktitle={Proceedings of the 17th International Joint Conference on Computer Vision, Imaging and Computer Graphics Theory and Applications - Volume 5: VISAPP,},
    year={2022},
    pages={428-436},
    publisher={SciTePress},
    organization={INSTICC},
}
```

# Acknowledgments
This work was supported by the Pack Ambition Recherche 2019 funding of the French AURA Region in
the context of the AutoBehave project.
<div style="text-align:center">
<img style="margin-right: 20px" src="assets/logo_liris.png" alt="LIRIS logo" height="75" width="160"/>
<img style="margin-left: 20px" src="assets/logo_ra.png" alt="RA logo" height="60" width="262"/>
</div>

