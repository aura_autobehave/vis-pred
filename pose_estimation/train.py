# ------------------------------------------------------------------------------
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.
# Written by Bin Xiao (Bin.Xiao@microsoft.com)
# ------------------------------------------------------------------------------

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
import pprint
import shutil
from collections import OrderedDict

import torch
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
from tensorboardX import SummaryWriter

import _init_paths
from core.config import config
from core.config import update_config
from core.config import update_dir
from core.config import get_model_name
from core.loss import JointsMSELoss, JointsMSELossVis
from core.function import train
from core.function import validate
from utils.utils import get_optimizer
from utils.utils import save_checkpoint
from utils.utils import create_logger

import dataset
import models


def parse_args():
    parser = argparse.ArgumentParser(description='Train keypoints network')
    # general
    parser.add_argument('--cfg',
                        help='experiment configure file name',
                        required=True,
                        type=str)

    args, rest = parser.parse_known_args()
    # update config
    update_config(args.cfg)

    # training
    parser.add_argument('--frequent',
                        help='frequency of logging',
                        default=config.PRINT_FREQ,
                        type=int)
    parser.add_argument('--gpus',
                        help='gpus',
                        type=str)
    parser.add_argument('--workers',
                        help='num of dataloader workers',
                        type=int)
    parser.add_argument('--resume',
                        action='store_true',
                        help='Resume')

    parser.add_argument('--model-file',
                        help='model state file',
                        type=str)

    parser.add_argument('--debug-memory', action='store_true')

    args = parser.parse_args()

    return args


def reset_config(config, args):
    if args.gpus:
        config.GPUS = args.gpus
    if args.workers:
        config.WORKERS = args.workers

    if args.resume:
        config.MODEL.INIT_WEIGHTS = False
        if args.model_file:
            config.TRAIN.CHECKPOINT = args.model_file

    if args.debug_memory:
        config.DEBUG.DEBUG_MEMORY = True


def main():
    args = parse_args()
    reset_config(config, args)

    logger, final_output_dir, tb_log_dir = create_logger(
        config, args.cfg, 'train')

    logger.info(pprint.pformat(args))
    logger.info(pprint.pformat(config))

    # cudnn related setting
    cudnn.benchmark = config.CUDNN.BENCHMARK
    torch.backends.cudnn.deterministic = config.CUDNN.DETERMINISTIC
    torch.backends.cudnn.enabled = config.CUDNN.ENABLED

    model = eval('models.' + config.MODEL.NAME + '.get_pose_net')(
        config, is_train=True
    )

    if args.resume:
        logger.info('=> loading model from {}'.format(config.TRAIN.CHECKPOINT))
        meta_info = torch.load(config.TRAIN.CHECKPOINT)

        # resume previous training
        state_dict = OrderedDict({k.replace('module.', ''): v
                                  for k, v in meta_info['state_dict'].items()})
        config.TRAIN.BEGIN_EPOCH = meta_info['epoch']
        model.load_state_dict(state_dict)

    else:
        '''
        logger.info('=> initialize from coco, so adjusting the last layer')
        model.final_layer = torch.nn.Conv2d(256, config.MODEL.NUM_JOINTS, (1, 1), (1, 1))
        logger.info('=> adjust done.')
        '''
        pass

    if config.TRAIN.FREEZE:
        model.freeze_encoder()
        model.freeze_deconv()

    # copy model file
    this_dir = os.path.dirname(__file__)
    shutil.copy2(
        os.path.join(this_dir, '../lib/models', config.MODEL.NAME + '.py'),
        final_output_dir)

    writer_dict = {
        'writer': SummaryWriter(log_dir=tb_log_dir),
        'train_global_steps': 0,
        'valid_global_steps': 0,
    }

    dump_input = torch.rand((config.TRAIN.BATCH_SIZE,
                             3,
                             config.MODEL.IMAGE_SIZE[1],
                             config.MODEL.IMAGE_SIZE[0]))
    writer_dict['writer'].add_graph(model, (dump_input,), verbose=False)

    gpus = [int(i) for i in config.GPUS.split(',')]
    model = torch.nn.DataParallel(model, device_ids=gpus).cuda()

    # define loss function (criterion) and optimizer
    if config.MODEL.PREDICT_VIS:
        class_weights = None
        if config.LOSS.USE_CLASS_WEIGHT:
            vis_weight = [1.61, 7.83, 1.]
            if config.MODEL.NB_VIS == 2:
                vis_weight = [(1 / vis_weight[1] + 1 / vis_weight[2]) * vis_weight[0], 1.]
            class_weights = torch.FloatTensor(vis_weight).cuda()
        criterion = JointsMSELossVis(
            use_target_weight=config.LOSS.USE_TARGET_WEIGHT,
            vis_ratio=config.LOSS.VIS_RATIO,
            vis_weight=class_weights
        ).cuda()
        for st in config.LOSS.VIS_STEP:
            if config.TRAIN.BEGIN_EPOCH >= st:
                criterion.update_vis_ratio(config.LOSS.VIS_FACTOR)
            else:
                break
    else:
        criterion = JointsMSELoss(
            use_target_weight=config.LOSS.USE_TARGET_WEIGHT
        ).cuda()

    optimizer = get_optimizer(config, model)

    lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(
        optimizer, config.TRAIN.LR_STEP, config.TRAIN.LR_FACTOR
    )

    # Data loading code
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])
    train_dataset = eval('dataset.' + config.DATASET.DATASET)(
        config,
        config.DATASET.ROOT,
        config.DATASET.TRAIN_SET,
        True,
        transforms.Compose([
            transforms.ToTensor(),
            normalize,
        ])
    )
    valid_dataset = eval('dataset.' + config.DATASET.DATASET)(
        config,
        config.DATASET.ROOT,
        config.DATASET.TEST_SET,
        False,
        transforms.Compose([
            transforms.ToTensor(),
            normalize,
        ])
    )

    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=config.TRAIN.BATCH_SIZE * len(gpus),
        shuffle=config.TRAIN.SHUFFLE,
        num_workers=config.WORKERS,
        pin_memory=True
    )
    valid_loader = torch.utils.data.DataLoader(
        valid_dataset,
        batch_size=config.TEST.BATCH_SIZE * len(gpus),
        shuffle=False,
        num_workers=config.WORKERS,
        pin_memory=True
    )

    best_perf = 0.0
    best_model = False
    for epoch in range(config.TRAIN.BEGIN_EPOCH, config.TRAIN.END_EPOCH):

        # Step Loss
        if config.MODEL.PREDICT_VIS and epoch in config.LOSS.VIS_STEP:
            criterion.update_vis_ratio(config.LOSS.VIS_FACTOR)

        # train for one epoch
        train(config, train_loader, model, criterion, optimizer, epoch,
              final_output_dir, tb_log_dir, writer_dict)

        lr_scheduler.step()

        # evaluate on validation set
        perf_indicator = validate(config, valid_loader, valid_dataset, model,
                                  criterion, final_output_dir, tb_log_dir,
                                  writer_dict)

        if perf_indicator > best_perf:
            best_perf = perf_indicator
            best_model = True
        else:
            best_model = False

        logger.info('=> saving checkpoint to {}'.format(final_output_dir))
        save_checkpoint({
            'epoch': epoch + 1,
            'model': get_model_name(config),
            'state_dict': model.state_dict(),
            'perf': perf_indicator,
            'optimizer': optimizer.state_dict(),
        }, best_model, final_output_dir)

        if config.TRAIN.SAVE_CHECKPOINT and not (epoch + 1) % config.TRAIN.SAVE_CHECKPOINT:
            shutil.copy2(os.path.join(final_output_dir, 'checkpoint.pth.tar'),
                         os.path.join(final_output_dir, f'checkpoint_{epoch + 1}.pth.tar'))

    final_model_state_file = os.path.join(final_output_dir,
                                          'final_state.pth.tar')
    logger.info('saving final model state to {}'.format(
        final_model_state_file))
    torch.save(model.module.state_dict(), final_model_state_file)
    writer_dict['writer'].close()


if __name__ == '__main__':
    main()
