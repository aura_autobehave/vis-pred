# ------------------------------------------------------------------------------
# Copyright (c) Microsoft
# Licensed under the MIT License.
# Written by Bin Xiao (Bin.Xiao@microsoft.com)
# ------------------------------------------------------------------------------

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import logging

import torch
import torch.nn as nn
from collections import OrderedDict

from .modules import DeconvStage, PoseNet
from .resnet import ResNet, BasicBlock, Bottleneck
from utils.utils import convert_state_dict

logger = logging.getLogger(__name__)


class PoseResNet(PoseNet):
    def __init__(self, block, layers, cfg, **kwargs):
        super(PoseResNet, self).__init__()

        self.resnet = ResNet(block, layers, cfg, **kwargs)
        self.final_stage = DeconvStage(self.resnet.inplanes, cfg)

    def forward(self, x):
        x = self.resnet(x)
        x = self.final_stage(x)
        return x

    def freeze_encoder(self, freeze=True):
        self.resnet.freeze(freeze=freeze)
        logger.info("Encoder frozen")

    def freeze_deconv(self, freeze=True):
        self.final_stage.freeze(freeze=freeze)
        logger.info("Deconv frozen")

    def load_state_dict(self, state_dict, strict=True):
        state_dict = OrderedDict({k.replace('module.', ''): v
                                  for k, v in state_dict.items()})
        try:
            nn.Module.load_state_dict(self, state_dict=state_dict, strict=True)
        except RuntimeError as err:
            str_err = str(err)
            if 'Missing key(s)' not in str_err and 'Unexpected key(s)' not in str_err:
                raise err
            if strict:
                new_dict = convert_state_dict(state_dict, ['resnet', 'final_stage'], err)
            else:
                print('\n'.join(str(err).split('\n')[1:]))
                new_dict = state_dict
            nn.Module.load_state_dict(self, state_dict=new_dict, strict=strict)


resnet_spec = {18: (BasicBlock, [2, 2, 2, 2]),
               34: (BasicBlock, [3, 4, 6, 3]),
               50: (Bottleneck, [3, 4, 6, 3]),
               101: (Bottleneck, [3, 4, 23, 3]),
               152: (Bottleneck, [3, 8, 36, 3])}


def get_pose_net(cfg, is_train, **kwargs):
    num_layers = cfg.MODEL.EXTRA.NUM_LAYERS
    style = cfg.MODEL.STYLE

    block_class, layers = resnet_spec[num_layers]

    model = PoseResNet(block_class, layers, cfg, **kwargs)

    if is_train and cfg.MODEL.INIT_WEIGHTS:
        model.init_weights(cfg.MODEL.PRETRAINED)

    return model
