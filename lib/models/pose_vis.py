from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from .modules import DeconvStageVis
from .resnet import ResNet
from .pose_resnet import PoseResNet, resnet_spec


class PoseResNetVis(PoseResNet):
    def __init__(self, block, layers, cfg, **kwargs):
        super(PoseResNetVis, self).__init__(block, layers, cfg, **kwargs)
        self.nb_vis = 3

        self.resnet = ResNet(block, layers, cfg, **kwargs)
        self.final_stage = DeconvStageVis(self.resnet.inplanes, block, cfg)

    def forward(self, x):
        x = self.resnet(x)
        x, vis_preds = self.final_stage(x)

        return x, vis_preds


def get_pose_net(cfg, is_train, **kwargs):
    num_layers = cfg.MODEL.EXTRA.NUM_LAYERS
    style = cfg.MODEL.STYLE

    block_class, layers = resnet_spec[num_layers]

    if style == 'caffe':
        raise NotImplementedError('Caffe not handled')

    model = PoseResNetVis(block_class, layers, cfg, **kwargs)

    if is_train and cfg.MODEL.INIT_WEIGHTS:
        model.init_weights(cfg.MODEL.PRETRAINED)

    return model
