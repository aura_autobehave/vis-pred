# ------------------------------------------------------------------------------
# Copyright (c) Microsoft
# Licensed under the MIT License.
# Written by Bin Xiao (Bin.Xiao@microsoft.com)
# ------------------------------------------------------------------------------

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch.nn as nn
from abc import ABC, abstractmethod


class JointsLoss(nn.Module, ABC):
    def __init__(self, use_target_weight, use_vis=False):
        super(JointsLoss, self).__init__()
        self.criterion = None
        self.use_target_weight = use_target_weight
        self.use_vis = use_vis

    def forward(self, outputs, targets, target_weights):
        if self.use_vis:
            return self._forward(*outputs, *targets, target_weight=target_weights)
        else:
            return self._forward(outputs[0], targets[0], target_weight=target_weights)

    @abstractmethod
    def _forward(self, outputs, targets, target_weights):
        pass


class JointsMSELoss(JointsLoss):
    def __init__(self, use_target_weight):
        super(JointsMSELoss, self).__init__(use_target_weight, use_vis=False)
        self.criterion = nn.MSELoss(size_average=True)

    def _forward(self, output, target, target_weight):
        batch_size = output.size(0)
        num_joints = output.size(1)
        heatmaps_pred = output.reshape((batch_size, num_joints, -1)).split(1, 1)
        heatmaps_gt = target.reshape((batch_size, num_joints, -1)).split(1, 1)
        loss = 0

        for idx in range(num_joints):
            heatmap_pred = heatmaps_pred[idx].squeeze()
            heatmap_gt = heatmaps_gt[idx].squeeze()
            if self.use_target_weight:
                loss += 0.5 * self.criterion(
                    heatmap_pred.mul(target_weight[:, idx]),
                    heatmap_gt.mul(target_weight[:, idx])
                )
            else:
                loss += 0.5 * self.criterion(heatmap_pred, heatmap_gt)

        loss /= num_joints
        return loss, [loss]


class JointsMSELossVis(JointsLoss):
    def __init__(self, use_target_weight, vis_ratio=.5, vis_weight=None):
        super(JointsMSELossVis, self).__init__(use_target_weight, use_vis=True)
        self.criterion = nn.MSELoss(size_average=True)
        self.vis_criterion = nn.CrossEntropyLoss(weight=vis_weight, size_average=True)

        # Ratio loss_vis / loss_hm
        self.vis_ratio = vis_ratio

    def update_vis_ratio(self, factor):
        self.vis_ratio += factor
        if self.vis_ratio > 1:
            self.vis_ratio = 1.
        if self.vis_ratio < 0:
            self.vis_ratio = 0.

    def _forward(self, output, vis_preds, target, vis_gts, target_weight):
        batch_size = output.size(0)
        num_joints = output.size(1)
        heatmaps_pred = output.reshape((batch_size, num_joints, -1)).split(1, 1)
        heatmaps_gt = target.reshape((batch_size, num_joints, -1)).split(1, 1)

        vis_preds = vis_preds.split(1, 1)
        vis_gts = vis_gts.split(1, 1)

        loss = 0
        loss_vis = 0

        for idx in range(num_joints):
            heatmap_pred = heatmaps_pred[idx].squeeze(1)
            heatmap_gt = heatmaps_gt[idx].squeeze(1)
            vis_pred = vis_preds[idx].squeeze(1)
            vis_gt = vis_gts[idx].squeeze(1)

            if self.use_target_weight:
                loss += 0.5 * self.criterion(
                    heatmap_pred.mul(target_weight[:, idx]),
                    heatmap_gt.mul(target_weight[:, idx])
                )
            else:
                loss += 0.5 * self.criterion(heatmap_pred, heatmap_gt)

            # 2.5E-3 provides aroud 50/50 ratio
            l_vis = 0.5 * 2.5E-3 * self.vis_criterion(vis_pred, vis_gt)
            
            # print(l_vis)
            loss_vis += l_vis

        loss /= num_joints
        loss_vis /= num_joints
        return (loss * (1 - self.vis_ratio) + loss_vis * self.vis_ratio), [loss, loss_vis]
