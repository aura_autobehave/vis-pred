# ------------------------------------------------------------------------------
# Copyright (c) Microsoft
# Licensed under the MIT License.
# Written by Bin Xiao (Bin.Xiao@microsoft.com)
# ------------------------------------------------------------------------------

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import json
import logging
import sys
import time
import os

import numpy as np
import torch
import cv2

from core.config import get_model_name
from core.evaluate import accuracy, accuracy_vis
from core.inference import get_final_preds, get_max_preds
from utils.transforms import flip_back
from utils.vis import save_debug_images
from utils.debug import GradPlots, print_tensors

logger = logging.getLogger(__name__)


def train(config, train_loader, model, criterion, optimizer, epoch,
          output_dir, tb_log_dir, writer_dict):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    acc = AverageMeter()
    acc_vis = AverageMeter()
    losses_det = AverageMeters()

    # switch to train mode
    model.train()

    end = time.time()
    predict_vis = config.MODEL.PREDICT_VIS

    grad_plot = GradPlots(key='fc')
    for i, (input, target, target_weight, target_vis, meta) in enumerate(train_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        # compute output
        output = model(input)
        if predict_vis or type(output) == tuple:
            output, out_vis = output
        else:
            out_vis = None

        if config.MODEL.NB_VIS == 2:
            target_vis = target_vis > 0
            target_vis = target_vis.long()

        target = target.cuda(non_blocking=True)
        target_weight = target_weight.cuda(non_blocking=True)
        target_vis = target_vis.cuda(non_blocking=True)

        loss, loss_detail = criterion([output, out_vis], [target, target_vis], target_weight)

        if config.DEBUG.DEBUG_MEMORY:
            print_tensors()
            sys.exit(0)

        # compute gradient and do update step
        optimizer.zero_grad()
        loss.backward()

        # plot_grad_flow(model.named_parameters())
        # sys.exit(0)
        # grad_plot.save_grads(model.named_parameters())

        optimizer.step()

        # measure accuracy and record loss
        losses.update(loss.item(), input.size(0))
        losses_det.update(loss_detail, input.size(0))

        _, avg_acc, cnt, pred = accuracy(output.detach().cpu().numpy(),
                                         target.detach().cpu().numpy())
        acc.update(avg_acc, cnt)

        if predict_vis:
            accu_vis, _ = accuracy_vis(out_vis.detach().cpu().numpy(), target_vis.detach().cpu().numpy())
            acc_vis.update(accu_vis, input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % config.PRINT_FREQ == 0:
            msg = 'Epoch: [{0}][{1}/{2}]\t' \
                  'Time {batch_time.val:.3f}s ({batch_time.avg:.3f}s)\t' \
                  'Speed {speed:.1f} samples/s\t' \
                  'Data {data_time.val:.3f}s ({data_time.avg:.3f}s)\t' \
                  'Loss {loss.val:.5f} ({loss.avg:.5f})\t' \
                  f'Loss_Det {" ".join([f"{l.val:.5f}" for l in losses_det])} ({" ".join([f"{l.avg:.5f}" for l in losses_det])})\t' \
                  'Accuracy {acc.val:.3f} ({acc.avg:.3f})'.format(
                epoch, i, len(train_loader), batch_time=batch_time,
                speed=input.size(0) / batch_time.val,
                data_time=data_time, loss=losses, acc=acc)
            if predict_vis:
                msg += f'\tAccu_vis {acc_vis.val:.3f} ({acc_vis.avg:.3f})'
            logger.info(msg)

            writer = writer_dict['writer']
            global_steps = writer_dict['train_global_steps']
            writer.add_scalar('train_loss', losses.val, global_steps)
            writer.add_scalar('train_acc', acc.val, global_steps)
            writer_dict['train_global_steps'] = global_steps + 1

            prefix = '{}_{}'.format(os.path.join(output_dir, 'train'), i)
            save_debug_images(config, input, meta, target, pred * 4, output,
                              prefix)


def validate(config, val_loader, val_dataset, model, criterion, output_dir,
             tb_log_dir, writer_dict=None):
    batch_time = AverageMeter()
    losses = AverageMeter()
    acc = AverageMeter()
    acc_vis = AverageMeter()

    losses_det = AverageMeters()

    # switch to evaluate mode
    model.eval()

    num_samples = len(val_dataset)
    all_preds = np.zeros((num_samples, config.MODEL.NUM_JOINTS, 3),
                         dtype=np.float32)
    all_boxes = np.zeros((num_samples, 6))
    all_maps = np.zeros(
        (num_samples, config.MODEL.NUM_JOINTS, config.MODEL.EXTRA.HEATMAP_SIZE[1], config.MODEL.EXTRA.HEATMAP_SIZE[0]),
        dtype=np.float32)
    all_gts = np.zeros((num_samples, config.MODEL.NUM_JOINTS, 3), dtype=np.float32)
    all_gts_maps = np.zeros(
        (num_samples, config.MODEL.NUM_JOINTS, config.MODEL.EXTRA.HEATMAP_SIZE[1], config.MODEL.EXTRA.HEATMAP_SIZE[0]),
        dtype=np.float32)
    all_preds_vis = np.zeros((num_samples, config.MODEL.NUM_JOINTS, config.MODEL.NB_VIS), dtype=np.float32)
    all_gts_vis = np.zeros((num_samples, config.MODEL.NUM_JOINTS), dtype=np.int32)

    all_names = []
    image_path = []
    filenames = []
    imgnums = []
    idx = 0

    predict_vis = config.MODEL.PREDICT_VIS

    with torch.no_grad():
        end = time.time()
        for i, (input, target, target_weight, target_vis, meta) in enumerate(val_loader):
            # compute output
            output = model(input)
            if predict_vis or type(output) == tuple:
                output, out_vis = output
            else:
                out_vis = None

            if config.TEST.FLIP_TEST:
                # this part is ugly, because pytorch has not supported negative index
                # input_flipped = model(input[:, :, :, ::-1])
                input_flipped = np.flip(input.cpu().numpy(), 3).copy()
                input_flipped = torch.from_numpy(input_flipped).cuda()
                # compute output
                output_flipped = model(input_flipped)
                if predict_vis or type(output_flipped) == tuple:
                    output_flipped, out_vis_flipped = output_flipped
                    out_vis = (out_vis + out_vis_flipped) * 0.5
                else:
                    out_vis_flipped = None

                output_flipped = flip_back(output_flipped.cpu().numpy(),
                                           val_dataset.flip_pairs)
                output_flipped = torch.from_numpy(output_flipped.copy()).cuda()

                # feature is not aligned, shift flipped heatmap for higher accuracy
                if config.TEST.SHIFT_HEATMAP:
                    output_flipped[:, :, :, 1:] = \
                        output_flipped.clone()[:, :, :, 0:-1]
                    # output_flipped[:, :, :, 0] = 0

                output = (output + output_flipped) * 0.5

            if config.MODEL.NB_VIS == 2:
                target_vis = target_vis > 0
                target_vis = target_vis.long()

            if False:
                t = target_vis < 1
                for r in range(t.shape[0]):
                    for c in range(t.shape[1]):
                        if t[r][c]:
                            m = output[r][c].max()
                            if m > 0.6:
                                ti = int(time.time())
                                hm = output[r][c].mul(255).clamp(0, 255).byte().cpu().numpy()
                                img = input[r].mul(255).clamp(0, 255).byte().permute(1, 2, 0).cpu().numpy()
                                hmi = cv2.applyColorMap(cv2.resize(hm, (img.shape[1], img.shape[0])), cv2.COLORMAP_JET)
                                cv2.imwrite(f'temp/{c}_{r}_{m}.png', hmi * 0.7 + 0.3 * img)

            target = target.cuda(non_blocking=True)
            target_weight = target_weight.cuda(non_blocking=True)
            target_vis = target_vis.cuda(non_blocking=True)

            loss, loss_detail = criterion([output, out_vis], [target, target_vis], target_weight)

            num_images = input.size(0)
            # measure accuracy and record loss
            losses.update(loss.item(), num_images)
            losses_det.update(loss_detail, num_images)

            _, avg_acc, cnt, pred = accuracy(output.cpu().numpy(),
                                             target.cpu().numpy())

            acc.update(avg_acc, cnt)

            if predict_vis:
                accu_vis, _ = accuracy_vis(out_vis.cpu().numpy(), target_vis.cpu().numpy())
                acc_vis.update(accu_vis, num_images)

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            c = meta['center'].numpy()
            s = meta['scale'].numpy()
            score = meta['score'].numpy()

            output_np = output.clone().cpu().numpy()

            preds, maxvals = get_final_preds(
                config, output_np, c, s)

            target_cpu = target.clone().cpu().numpy()
            target_vis_cpu = target_vis.cpu().numpy()
            all_maps[idx:idx + num_images, :, :, :] = output_np
            all_gts_maps[idx:idx + num_images, :, :, :] = target_cpu
            all_gts[idx:idx + num_images, :, :2] = get_max_preds(target_cpu)[0]
            all_gts[idx:idx + num_images, :, 2] = target_vis_cpu
            all_names += [os.path.split(n)[1] for n in meta['image']]

            all_preds[idx:idx + num_images, :, 0:2] = preds[:, :, 0:2]
            all_preds[idx:idx + num_images, :, 2:3] = maxvals
            # double check this all_boxes parts
            all_boxes[idx:idx + num_images, 0:2] = c[:, 0:2]
            all_boxes[idx:idx + num_images, 2:4] = s[:, 0:2]
            all_boxes[idx:idx + num_images, 4] = np.prod(s * 200, 1)
            all_boxes[idx:idx + num_images, 5] = score
            image_path.extend(meta['image'])

            if predict_vis:
                all_preds_vis[idx:idx + num_images, :, :] = out_vis.cpu().numpy()
                all_gts_vis[idx:idx + num_images, :] = target_vis_cpu

            if config.DATASET.DATASET == 'posetrack':
                filenames.extend(meta['filename'])
                imgnums.extend(meta['imgnum'].numpy())

            idx += num_images

            if i % config.PRINT_FREQ == 0:
                msg = 'Test: [{0}/{1}]\t' \
                      'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t' \
                      'Loss {loss.val:.5f} ({loss.avg:.5f})\t' \
                      f'Loss_Det {" ".join([f"{l.val:.5f}" for l in losses_det])} ({" ".join([f"{l.avg:.5f}" for l in losses_det])})\t' \
                      'Accuracy {acc.val:.3f} ({acc.avg:.3f})'.format(
                    i, len(val_loader), batch_time=batch_time,
                    loss=losses, acc=acc)
                if predict_vis:
                    msg += f'\tAccu_vis {acc_vis.val:.3f} ({acc_vis.avg:.3f})'
                logger.info(msg)

                prefix = '{}_{}'.format(os.path.join(output_dir, 'val'), i)
                save_debug_images(config, input, meta, target, pred * 4, output,
                                  prefix)

        name_values, perf_indicator = val_dataset.evaluate(
            config, all_preds, output_dir, all_boxes, image_path,
            filenames, imgnums)

        with open(os.path.join(output_dir, 'results', 'keypoints_%s_preds.json' % val_dataset.image_set), 'w') as f:
            json.dump({'annotations': all_preds.tolist(), 'names': all_names}, f)

        with open(os.path.join(output_dir, 'results', 'keypoints_%s_gts.json' % val_dataset.image_set), 'w') as f:
            json.dump({'annotations': all_gts.tolist(), 'names': all_names}, f)

        if predict_vis:
            accu_vis, flat_out_vis = accuracy_vis(all_preds_vis, all_gts_vis, f1=True,
                                                  save=os.path.join(output_dir, 'results', 'result_vis_val.json'))
            logger.info(f"Total vis accuracy: {accu_vis:.3f}")

            all_vis_filter = flat_out_vis.reshape((-1, 17)) > 0
            all_preds_nnz = all_preds.copy()
            for i in range(all_preds_nnz.shape[-1]):
                all_preds_nnz[:, :, i] *= all_vis_filter

            res_file = os.path.join(
                output_dir, 'results', 'keypoints_%s_results_nnz.json' % val_dataset.image_set)

            val_dataset.evaluate(
                config, all_preds_nnz, output_dir, all_boxes, image_path,
                filenames, imgnums, res_file=res_file)

        # np.save('val_out.npy', all_maps)
        # np.savez_compressed('eval_out', names=all_names, maps=all_maps)
        # np.savez_compressed('eval_gts', names=all_names, maps=all_gts_maps)

        _, full_arch_name = get_model_name(config)
        if isinstance(name_values, list):
            for name_value in name_values:
                _print_name_value(name_value, full_arch_name)
        else:
            _print_name_value(name_values, full_arch_name)

        if writer_dict:
            writer = writer_dict['writer']
            global_steps = writer_dict['valid_global_steps']
            writer.add_scalar('valid_loss', losses.avg, global_steps)
            writer.add_scalar('valid_acc', acc.avg, global_steps)
            if isinstance(name_values, list):
                for name_value in name_values:
                    writer.add_scalars('valid', dict(name_value), global_steps)
            else:
                writer.add_scalars('valid', dict(name_values), global_steps)
            writer_dict['valid_global_steps'] = global_steps + 1

    return perf_indicator


# markdown format output
def _print_name_value(name_value, full_arch_name):
    names = name_value.keys()
    values = name_value.values()
    num_values = len(name_value)
    logger.info(
        '| Arch ' +
        ' '.join(['| {}'.format(name) for name in names]) +
        ' |'
    )
    logger.info('|---' * (num_values + 1) + '|')
    logger.info(
        '| ' + full_arch_name + ' ' +
        ' '.join(['| {:.3f}'.format(value) for value in values]) +
        ' |'
    )


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count if self.count != 0 else 0


class AverageMeters(object):
    def __init__(self):
        self.meters = []

    def update(self, vals, n=1):
        if len(self.meters) < len(vals):
            self.meters = [AverageMeter() for _ in vals]

        for i, val in enumerate(vals):
            self.meters[i].update(val.item(), n)

    def reset(self):
        for met in self.meters:
            met.reset()

    def __iter__(self):
        for met in self.meters:
            yield met
