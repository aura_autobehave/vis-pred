# ------------------------------------------------------------------------------
# Copyright (c) Microsoft
# Licensed under the MIT License.
# Written by Bin Xiao (Bin.Xiao@microsoft.com)
# ------------------------------------------------------------------------------

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import json
import logging

import numpy as np
from sklearn.metrics import f1_score, precision_score, recall_score

from core.inference import get_max_preds
from utils.tabs import Tabs

logger = logging.getLogger(__name__)


def calc_dists(preds, target, normalize):
    preds = preds.astype(np.float32)
    target = target.astype(np.float32)
    dists = np.zeros((preds.shape[1], preds.shape[0]))
    for n in range(preds.shape[0]):
        for c in range(preds.shape[1]):
            if target[n, c, 0] > 1 and target[n, c, 1] > 1:
                normed_preds = preds[n, c, :] / normalize[n]
                normed_targets = target[n, c, :] / normalize[n]
                dists[c, n] = np.linalg.norm(normed_preds - normed_targets)
            else:
                dists[c, n] = -1
    return dists


def dist_acc(dists, thr=0.5):
    ''' Return percentage below threshold while ignoring values with a -1 '''
    dist_cal = np.not_equal(dists, -1)
    num_dist_cal = dist_cal.sum()
    if num_dist_cal > 0:
        return np.less(dists[dist_cal], thr).sum() * 1.0 / num_dist_cal
    else:
        return -1


def accuracy(output, target, hm_type='gaussian', thr=0.5):
    '''
    Calculate accuracy according to PCK,
    but uses ground truth heatmap rather than x,y locations
    First value to be returned is average accuracy across 'idxs',
    followed by individual accuracies
    '''
    idx = list(range(output.shape[1]))
    norm = 1.0
    if hm_type == 'gaussian':
        pred, _ = get_max_preds(output)
        target, _ = get_max_preds(target)
        h = output.shape[2]
        w = output.shape[3]
        norm = np.ones((pred.shape[0], 2)) * np.array([h, w]) / 10
    dists = calc_dists(pred, target, norm)

    acc = np.zeros((len(idx) + 1))
    avg_acc = 0
    cnt = 0

    for i in range(len(idx)):
        acc[i + 1] = dist_acc(dists[idx[i]])
        if acc[i + 1] >= 0:
            avg_acc = avg_acc + acc[i + 1]
            cnt += 1

    avg_acc = avg_acc / cnt if cnt != 0 else 0
    if cnt != 0:
        acc[0] = avg_acc
    return acc, avg_acc, cnt, pred


def accuracy_vis(output, target, f1=False, save=None):
    '''
    Calculate accuracy according to PCK for visibilty
    :param output: Prediction numpy array: either class (nb_preds, nb_joints) or model output (nb_preds, nb_joints, nb_classes)
    :param target: Ground-truth numpy array (nb_preds, nb_joints)
    :param f1: Boolean : compute and display or not f1 score
    :param target: Path to save predictions and results to json
    :return: accuracy
    '''

    lbls = [[i] for i in range(output.shape[2])] + [None]

    output_preds = output
    if len(output.shape) > 2:
        output_preds = output.argmax(axis=2)
    corrects = output_preds == target

    if f1:
        avg_mode = 'weighted'
        flat_targ = target.flatten()
        flat_out = output_preds.flatten()
        prec_scs = []
        reca_scs = []
        f1_scs = []
        for lb in lbls:
            prec_scs.append(precision_score(flat_targ, flat_out, labels=lb, average=avg_mode, zero_division=0))
            reca_scs.append(recall_score(flat_targ, flat_out, labels=lb, average=avg_mode, zero_division=0))
            f1_scs.append(f1_score(flat_targ, flat_out, labels=lb, average=avg_mode, zero_division=0))

        Tabs([prec_scs, reca_scs, f1_scs], lbls_r=['Prec', 'Reca', 'F1'], lbls_c=[str(lb) for lb in lbls],
             logger=logger)

    if save is not None:
        with open(save, 'w') as f_out:
            json.dump(dict([(l[0], l[1].tolist()) for l in [('target', target), ('output', output)]]), f_out)

    return corrects.mean(), flat_out if f1 else None
