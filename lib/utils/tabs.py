import numpy as np


def text_from_data(data, lbls_r=None, lbls_c=None):
    text = ''
    if not data:
        return text

    if not type(data[0]) in (list, dict, np.ndarray):
        data = [data]

    if lbls_c:
        text = '\t' if lbls_r else ''
        text += '\t'.join(lbls_c)

    for r, row in enumerate(data):
        if lbls_c or r:
            text += '\n'
        if lbls_r:
            text += lbls_r[r] + '\t'

        text += '\t'.join([f'{x:0.2f}' if type(x) in [float, np.float_] else f'{x}' for x in row])

    return text


class Tabs:
    def __init__(self, data, lbls_r=None, lbls_c=None, logger=None):
        if not data:
            return
        self.text = data if type(data) == str else text_from_data(data, lbls_r=lbls_r, lbls_c=lbls_c)

        self.rows = [l.split('\t') for l in self.text.split('\n')]
        if self.text[-1] == '\n':
            self.rows.pop()

        self.num_l = len(self.rows)
        self.num_c = len(self.rows[0]) if self.rows else 0

        self.cols = [[l[i] if len(l) > i else '' for l in self.rows] for i in range(self.num_c)]

        self.len_c = [max([len(x) for x in c]) for c in self.cols]

        self.logger = logger
        self.disp_text = self.disp()

    def disp(self):
        disp_text = ''
        for row in self.rows:
            for c, w in enumerate(row):
                disp_text += '\t' if c > 0 else ''
                disp_text += w + ' ' * (self.len_c[c] - len(w))
            disp_text += '\n'

        if self.logger:
            self.logger.info('\n' + disp_text)
        else:
            print(disp_text)
        return disp_text

    def to_latex(self):
        header = """
        \\begin{table}[!htb]
        \\centering
        \\small
        \\renewcommand{\\tabcolsep}{2pt}
        \\begin{tabular}{"""

        header2 = ''.join(['c' + (' ' if c else '|') for c in range(self.num_c)])
        header3 = """}
        \\hline
        """

        header = header + header2 + header3

        feat = """\\hline        
        \\multicolumn{9}{c}{}
        \\end{tabular}
        \\caption{Comparisons of AP scores on the COCO 2017 val set with AP OKS.}
        \\label{tab:cocotest}
        \\end{table}"""

        mid = ''
        for r, row in enumerate(self.rows):
            for c, w in enumerate(row):
                mid += ' & ' if c > 0 else ''
                mid += w
            mid += '\\\\\n'
            if not r:
                mid += "\\hline\n"

        tab = header + mid + feat
        print(tab.replace('        ', ''))


if __name__ == '__main__':
    lbl_c = ['AP', 'AP$^{50}$', 'AP$^{75}$', 'AP$^L$', 'AR', 'AR$^{50}$', 'AR$^{75}$', 'AR$^L$']
    lbl_r = [r'SBl~\cite{Xiao_2018_ECCV}', r'MSPN~\cite{MSPN_2019}', 'RSN~\cite{RSN_2020}']
    data = [[.72, .92, .80, .77, .76, .93, .82, .80],
            [.77, .94, .85, .82, .80, .95, .87, .85],
            [.76, .94, .84, .81, .79, .94, .85, .84]]
    t = Tabs(data, lbls_r=lbl_r, lbls_c=lbl_c)
    t.to_latex()
