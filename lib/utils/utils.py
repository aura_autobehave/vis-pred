# ------------------------------------------------------------------------------
# Copyright (c) Microsoft
# Licensed under the MIT License.
# Written by Bin Xiao (Bin.Xiao@microsoft.com)
# ------------------------------------------------------------------------------

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import logging
import sys
import time
from pathlib import Path
from collections import OrderedDict

import torch
import torch.optim as optim

from core.config import get_model_name


def create_logger(cfg, cfg_name, phase='train'):
    root_output_dir = Path(cfg.OUTPUT_DIR)
    # set up logger
    if not root_output_dir.exists():
        print('=> creating {}'.format(root_output_dir))
        root_output_dir.mkdir()

    dataset = cfg.DATASET.DATASET + '_' + cfg.DATASET.HYBRID_JOINTS_TYPE \
        if cfg.DATASET.HYBRID_JOINTS_TYPE else cfg.DATASET.DATASET
    dataset = dataset.replace(':', '_')
    model, _ = get_model_name(cfg)
    cfg_name = os.path.basename(cfg_name).split('.')[0]

    final_output_dir = root_output_dir / dataset / model / cfg_name

    print('=> creating {}'.format(final_output_dir))
    final_output_dir.mkdir(parents=True, exist_ok=True)

    time_str = time.strftime('%Y-%m-%d-%H-%M')
    log_file = '{}_{}_{}.log'.format(cfg_name, time_str, phase)
    final_log_file = final_output_dir / log_file
    head = '%(asctime)-15s %(message)s'
    logging.basicConfig(filename=str(final_log_file),
                        format=head)
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    console = logging.StreamHandler(sys.stdout)
    logging.getLogger('').addHandler(console)

    tensorboard_log_dir = Path(cfg.LOG_DIR) / dataset / model / \
                          (cfg_name + '_' + time_str)
    print('=> creating {}'.format(tensorboard_log_dir))
    tensorboard_log_dir.mkdir(parents=True, exist_ok=True)

    return logger, str(final_output_dir), str(tensorboard_log_dir)


def get_optimizer(cfg, model):
    optimizer = None
    if cfg.TRAIN.OPTIMIZER == 'sgd':
        optimizer = optim.SGD(
            model.parameters(),
            lr=cfg.TRAIN.LR,
            momentum=cfg.TRAIN.MOMENTUM,
            weight_decay=cfg.TRAIN.WD,
            nesterov=cfg.TRAIN.NESTEROV
        )
    elif cfg.TRAIN.OPTIMIZER == 'adam':
        optimizer = optim.Adam(
            model.parameters(),
            lr=cfg.TRAIN.LR
        )

    return optimizer


def save_checkpoint(states, is_best, output_dir,
                    filename='checkpoint.pth.tar'):
    torch.save(states, os.path.join(output_dir, filename))
    if is_best and 'state_dict' in states:
        torch.save(states['state_dict'],
                   os.path.join(output_dir, 'model_best.pth.tar'))


def convert_state_dict(state_dict, keys, error):
    msg = str(error)
    for m in ['Error(s) in loading state_dict for', 'Missing key(s)', 'Unexpected key(s)']:
        if m not in msg:
            raise error

    lines = msg.split('\n')
    missing = []
    unexpected = []
    for i, line in enumerate(lines[1:]):
        storage = (missing, unexpected)[i]
        for word in line.split():
            if '"' not in word:
                continue
            storage.append(word[1:-2])

    if len(missing) > len(unexpected):
        raise error

    new_dict = OrderedDict()
    for k, v in state_dict.items():
        if k not in unexpected:
            new_dict[k] = v
            continue
        elif '.num_batches_tracked' in k:
            continue
        for i, m in enumerate(missing):
            split_m = m.split('.')
            short_m = '.'.join(split_m[1:])
            if short_m == k and split_m[0] in keys:
                new_dict[m] = v

                if '.running_var' in m:
                    nbt = m.replace('.running_var', '.num_batches_tracked')
                    new_dict[nbt] = state_dict[k.replace('.running_var', '.num_batches_tracked')]

                missing.pop(i)
                break
        else:
            raise error

    return OrderedDict(new_dict)
