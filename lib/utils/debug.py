import matplotlib.pyplot as plt
import numpy as np
import gc, torch


class GradPlots:
    def __init__(self, key=None):
        self.layers = []
        self.ave_grads = {}
        self.epochs = 0
        self.key = key

    def save_grads(self, named_parameters):
        ave_grads = []
        layers = []
        for n, p in named_parameters:
            if self.key in n:
                print(n)
            if (p.requires_grad) and ("bias" not in n):
                layers.append(n)
                ave_grads.append(p.grad.abs().mean())

        np_grads = np.asarray(ave_grads)
        if self.key is None:
            # Keep 10 greatest
            idx = np_grads.argpartition(-10)[-10:]
        else:
            idx = [i for i, k in enumerate(layers) if self.key in k]
        ave_grads = np_grads[idx].tolist()
        layers = np.asarray(layers)[idx].tolist()

        for l, layer in enumerate(layers):
            if layer in self.layers:
                self.ave_grads[layer].append(ave_grads[l])
            else:
                self.ave_grads[layer] = [ave_grads[l]]
                self.layers.append(layer)

        self.epochs += 1

        for k in self.ave_grads:
            while len(self.ave_grads[k]) < self.epochs:
                self.ave_grads[k] = [0] + self.ave_grads[k]

    def plot_graph(self):
        for e in range(self.epochs):
            ave_grad = [self.ave_grads[l][e] for l in self.layers]

            plt.plot(ave_grad, alpha=0.3, label=f'{e}')

        plt.hlines(0, 0, len(self.layers) + 1, linewidth=1, color="k")
        plt.xticks(range(0, len(self.layers), 1), self.layers, rotation="vertical")
        plt.xlim(xmin=0, xmax=len(self.layers))
        plt.xlabel("Layers")
        plt.ylabel("average gradient")
        plt.title("Gradient flow")
        plt.grid(True)
        plt.legend()
        plt.savefig('debug_grad.png', bbox_inches='tight')


def print_tensors():
    tensors = {}
    for obj in gc.get_objects():
        try:
            if torch.is_tensor(obj) or (hasattr(obj, 'data') and torch.is_tensor(obj.data)):
                lbl_type = type(obj)
                if lbl_type == torch.nn.parameter.Parameter:
                    lbl_type = 'parameter'
                elif lbl_type == torch.Tensor:
                    lbl_type = 'tensor'

                mem_size = obj.element_size() * obj.nelement()
                lbls_mem = ['', 'k', 'M', 'G', 'T', 'P']
                i = 0
                while mem_size >= 1024:
                    mem_size /= 1024
                    i += 1
                tensors[
                    obj.element_size() * obj.nelement()] = f'{lbl_type}: {tuple(obj.size())}, {mem_size:.2}{lbls_mem[i]}b'
        except:
            pass
    for k in sorted(tensors, reverse=True):
        print(tensors[k])
